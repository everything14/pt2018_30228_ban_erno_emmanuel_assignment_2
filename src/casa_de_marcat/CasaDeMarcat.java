package casa_de_marcat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.PriorityQueue;
import java.util.concurrent.locks.Lock;

import client_package.Client;
import client_package.GenerareClienti;

public class CasaDeMarcat extends Thread {
	String afisare_textPane=new String();

	private String nume_fisier_log;
	private String nume;
	private Lock lock;
	private PriorityQueue<Client> clients;
	private float asteptare = 100000;
	private PrintWriter writer;
	
	public CasaDeMarcat(String nume, Lock l, PriorityQueue<Client> clients) {
		this.nume = nume;
		this.lock = l;
		this.clients = clients;
	}

	public void run() {
		try {
			this.writer = new PrintWriter(this.nume_fisier_log, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		while (clients.isEmpty() == false) {
			Client alege_client = clients.poll();
			afisare_consola(nume + ": A intrat un client in coada " + alege_client.getName());
			String aux1=nume + ": A intrat un client in coada " + alege_client.getName();
			afisare_textPane+=aux1+"\n";
			writer.println(aux1+"\n");
			this.lock.lock();
			statLaCoada(alege_client);
			
			afisare_consola(nume + ": A intrat un client la casa " + alege_client.getName());
			String aux2=nume + ": A intrat un client la casa " + alege_client.getName();
			afisare_textPane+=aux2+"\n";
			writer.println(aux2+"\n");
			statLaCasa(alege_client);
			this.lock.unlock();
			
			afisare_consola(nume + ": A iesit clientul de la casa " + alege_client.getName());
			String aux3=nume + ": A iesit clientul de la casa " + alege_client.getName();
			afisare_textPane+=aux3+"\n";
			writer.println(aux3+"\n");
		}
		writer.checkError();
		writer.close();
	}

	private void statLaCoada(Client client) {
		try {
			Thread.sleep(
					Math.round(Math.random() * (client.getCoada_max() - client.getCoada_min()) + client.getCoada_min())
							* 500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void statLaCasa(Client client) {
		int k = (int) Math.round(Math.random() * (client.getCasa_max() - client.getCasa_min()) + client.getCasa_min());
		for (int i = 0; i < k * asteptare; i++) {
			i++;
			i--;
		}
	}

	public void scriere_in_fisier(String linie_noua) throws FileNotFoundException, UnsupportedEncodingException {
		BufferedWriter writer_buf = null;			
		writer.println("The second line");
	}

	public String getNume_fisier_log() {
		return nume_fisier_log;
	}

	public void setNume_fisier_log(String nume_fisier_log) {
		this.nume_fisier_log = nume_fisier_log;
	}

	public String getAfisare_textPane() {
		return afisare_textPane;
	}

	public void setAfisare_textPane(String afisare_textPane) {
		this.afisare_textPane = afisare_textPane;
	}
	
	public void afisare_consola(String mesaj) {
		System.out.println(mesaj);
	}
	

}
