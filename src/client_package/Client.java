package client_package;

 public class Client implements Comparable<Object> {
	 
	private String name;
	private int coada_min; 
	private int coada_max; 
	private int casa_min;
	private int casa_max;
	
	
	public Client(int coada_min, int coada_max, int casa_min, int casa_max,String name) {

		this.coada_min = coada_min;
		this.coada_max = coada_max;
		this.casa_min = casa_min;
		this.casa_max = casa_max;
		this.name=name;
	}
	
	public int getCoada_min() {
		return coada_min;
	}
	public void setCoada_min(int coada_min) {
		this.coada_min = coada_min;
	}
	public int getCoada_max() {
		return coada_max;
	}
	public void setCoada_max(int coada_max) {
		this.coada_max = coada_max;
	}
	public int getCasa_min() {
		return casa_min;
	}
	public void setCasa_min(int casa_min) {
		this.casa_min = casa_min;
	}
	public int getCasa_max() {
		return casa_max;
	}
	public void setCasa_max(int casa_max) {
		this.casa_max = casa_max;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString() {
		return null;
	}

	

}
