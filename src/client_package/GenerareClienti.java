package client_package;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

public class GenerareClienti {

	private int coada_value;
	private int casa_value;
	Random rand;
	
	private List<String> nume_clienti = new ArrayList<String>();
	private int coada_min;
	private int coada_max;
	private int casa_min;
	private int casa_max;
	private int numarClienti;
	private PriorityQueue<Client> lista_clienti = new PriorityQueue<Client>();

	public void initialise_clients() {
		populate_list();
		int coada_random=generate_random(coada_value);		
		int casa_random=generate_random(casa_value);	
		
		
		this.coada_min=coada_random;
		this.coada_max=coada_random+2;
		
		this.casa_max=casa_random+2;
		this.casa_min=casa_random;
		
		for(int i=0;i<numarClienti;i++) {
			rand=new Random(54);
			lista_clienti.add(new Client(this.coada_min,this.coada_max,this.casa_min,this.casa_max,nume_clienti.get(generate_random(54))));
		}
		
	}

	public int generate_random(int value) {
		Random randomGenerator = new Random();		
		int randomInt = randomGenerator.nextInt(value);	
		return randomInt;
	}

	public void populate_list() {
		this.nume_clienti.add("Malone");
		this.nume_clienti.add("Peperoni");
		this.nume_clienti.add("Gigel");
		this.nume_clienti.add("Sclav");
		this.nume_clienti.add("Alupigus");
		this.nume_clienti.add("Salmonela");
		this.nume_clienti.add("Gigi");
		this.nume_clienti.add("Brusli");
		this.nume_clienti.add("Jamal");	
		this.nume_clienti.add("Tarnacop");
		this.nume_clienti.add("Bacu");
		this.nume_clienti.add("Marian");
		this.nume_clienti.add("Vulpe");
		this.nume_clienti.add("Lupu");
		this.nume_clienti.add("Cioara");
		this.nume_clienti.add("Batman");
		this.nume_clienti.add("Cristiiii");
		this.nume_clienti.add("Emilut");
		this.nume_clienti.add("Horia");
		this.nume_clienti.add("Nesti");
		this.nume_clienti.add("Boske");
		this.nume_clienti.add("Man");
		this.nume_clienti.add("Unguru'");
		this.nume_clienti.add("Kecske");
		this.nume_clienti.add("Roli Poli Oli");
		this.nume_clienti.add("Olli");
		this.nume_clienti.add("Bazatu");
		this.nume_clienti.add("Frumusikha");
		this.nume_clienti.add("Salam");
		this.nume_clienti.add("Mocanu");
		this.nume_clienti.add("Syn Gates");
		this.nume_clienti.add("Corey");
		this.nume_clienti.add("Nimeni");
		this.nume_clienti.add("Katiusha");
		this.nume_clienti.add("Mircea Bravo");
		this.nume_clienti.add("Cineva");
		this.nume_clienti.add("Ping Pong");
		this.nume_clienti.add("KV1");
		this.nume_clienti.add("IS7");
		this.nume_clienti.add("Soferu' de buldozer");
		this.nume_clienti.add("Micutu");
		this.nume_clienti.add("Studentu sarac");
		this.nume_clienti.add("Studentu bogat");
		this.nume_clienti.add("Angajatu");
		this.nume_clienti.add("Lenesu");
		this.nume_clienti.add("Joe Jegosu");
		this.nume_clienti.add("Dexter");
		this.nume_clienti.add("Bugs Bunny");
		this.nume_clienti.add("Trump");
		this.nume_clienti.add("Kashmir");
		this.nume_clienti.add("Elisei");
		this.nume_clienti.add("Emmanuel");
		this.nume_clienti.add("Banut");
		this.nume_clienti.add("Erno");
		this.nume_clienti.add("Nu mai stiu");

	}

	public boolean isListEmpty() {
		return this.lista_clienti.isEmpty();
	}
	public int list_size() {
		return lista_clienti.size();
	}
	public boolean list_contains_client(Client givenClient) {
		return this.lista_clienti.contains(givenClient);
	}
	/**
	 * @return the nume_clienti
	 */
	public List<String> getNume_clienti() {
		return nume_clienti;
	}

	/**
	 * @param nume_clienti
	 *            the nume_clienti to set
	 */
	public void setNume_clienti(List<String> nume_clienti) {
		this.nume_clienti = nume_clienti;
	}

	/**
	 * @return the coada_min
	 */
	public int getCoada_min() {
		return coada_min;
	}

	/**
	 * @param coada_min
	 *            the coada_min to set
	 */
	public void setCoada_min(int coada_min) {
		this.coada_min = coada_min;
	}

	/**
	 * @return the coada_max
	 */
	public int getCoada_max() {
		return coada_max;
	}

	/**
	 * @param coada_max
	 *            the coada_max to set
	 */
	public void setCoada_max(int coada_max) {
		this.coada_max = coada_max;
	}

	/**
	 * @return the casa_min
	 */
	public int getCasa_min() {
		return casa_min;
	}

	/**
	 * @param casa_min
	 *            the casa_min to set
	 */
	public void setCasa_min(int casa_min) {
		this.casa_min = casa_min;
	}

	/**
	 * @return the casa_max
	 */
	public int getCasa_max() {
		return casa_max;
	}

	/**
	 * @param casa_max
	 *            the casa_max to set
	 */
	public void setCasa_max(int casa_max) {
		this.casa_max = casa_max;
	}

	/**
	 * @return the coada_value
	 */
	public int getCoada_value() {
		return coada_value;
	}

	/**
	 * @param coada_value the coada_value to set
	 */
	public void setCoada_value(int coada_value) {
		this.coada_value = coada_value;
	}

	/**
	 * @return the casa_value
	 */
	public int getCasa_value() {
		return casa_value;
	}

	/**
	 * @param casa_value the casa_value to set
	 */
	public void setCasa_value(int casa_value) {
		this.casa_value = casa_value;
	}

	/**
	 * @return the numarClienti
	 */
	public int getNumarClienti() {
		return numarClienti;
	}

	/**
	 * @param numarClienti the numarClienti to set
	 */
	public void setNumarClienti(int numarClienti) {
		this.numarClienti = numarClienti;
	}

	/**
	 * @return the lista_clienti
	 */
	public PriorityQueue<Client> getLista_clienti() {
		return lista_clienti;
	}

	/**
	 * @param lista_clienti the lista_clienti to set
	 */
	public void setLista_clienti(PriorityQueue<Client> lista_clienti) {
		this.lista_clienti = lista_clienti;
	}
	
	

}
