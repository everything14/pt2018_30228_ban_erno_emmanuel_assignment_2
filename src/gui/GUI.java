package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;

import casa_de_marcat.CasaDeMarcat;
import client_package.Client;
import client_package.GenerareClienti;

import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.PriorityQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.Timer;

public class GUI {
	
	CasaDeMarcat f1, f2;
	private JFrame frame;
	private JTextField textField_Coada;
	private JTextField textField_casa;
	private JTextField textField_totalClienti;
	private JTextPane textPane_simulare = new JTextPane();
	private JProgressBar progressBar = new JProgressBar();
	PriorityQueue<Client> clients = new PriorityQueue<Client>();
	

	float valoare_progress_bar=0;

	/**
	 * @wbp.parser.entryPoint
	 */
	public void initializare_gui() {
		textField_totalClienti = new JTextField();
		JLabel lblNumarClienti = new JLabel("Numar clienti");
		JLabel lblNewLabel = new JLabel("SIMULARE COZI MAGAZIN");
		JLabel lblSimulationResults = new JLabel("SIMULATION RESULTS");
		JLabel lblTimpLaCoada = new JLabel("Timp la coada");
		JLabel label_timpCasa = new JLabel("Timp la casa");
		textField_casa = new JTextField();
		textField_Coada = new JTextField();
	 
		JButton btnStart = new JButton("START");
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 778, 573);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		lblNewLabel.setFont(new Font("Yu Gothic UI", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel.setBounds(190, 11, 246, 40);
		frame.getContentPane().add(lblNewLabel);
		
		
		textPane_simulare.setBackground(SystemColor.scrollbar);
		textPane_simulare.setBounds(406, 78, 346, 426);
		frame.getContentPane().add(textPane_simulare);
		
		lblSimulationResults.setFont(new Font("Sitka Subheading", Font.BOLD | Font.ITALIC, 17));
		lblSimulationResults.setForeground(SystemColor.textHighlight);
		lblSimulationResults.setBounds(495, 53, 206, 14);
		frame.getContentPane().add(lblSimulationResults);
		
		progressBar.setBounds(34, 442, 272, 14);
		progressBar.setStringPainted(true);
		frame.getContentPane().add(progressBar);
		
		btnStart.setBackground(SystemColor.textHighlight);
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Lock l = new ReentrantLock();
				
				GenerareClienti generate=new GenerareClienti();

				
				
				generate.setCasa_value(Integer.parseInt(textField_casa.getText()));
				generate.setCoada_value(Integer.parseInt(textField_Coada.getText()));
				generate.setNumarClienti(Integer.parseInt(textField_totalClienti.getText()));
				generate.initialise_clients();
				clients=generate.getLista_clienti();
				f1=new CasaDeMarcat("Casa1",l,clients);
				f2=new CasaDeMarcat("Casa2",l,clients);
							
				
				f1.setNume_fisier_log("casa1.txt");
				f2.setNume_fisier_log("casa2.txt");
				
				f1.start();
				f2.start();
				
				int intarziere = 50;
				
				
			    ActionListener afisare = new ActionListener() {
			        public void actionPerformed(ActionEvent evt) {	  	         				
			        	
			        	textPane_simulare.setText(f2.getAfisare_textPane());
			        	textPane_simulare.setText(f1.getAfisare_textPane());   
			        	valoare_progress_bar=(float) (valoare_progress_bar+0.2);
			        	progressBar.setValue((int)valoare_progress_bar);
			            	       
			        }
			    };
			    new Timer(intarziere, afisare).start(); 
				
			}
		});
		btnStart.setBounds(151, 341, 89, 23);
		frame.getContentPane().add(btnStart);
		
		
		textField_Coada.setBounds(169, 152, 115, 20);
		frame.getContentPane().add(textField_Coada);
		textField_Coada.setColumns(10);
		
		
		textField_casa.setBounds(169, 211, 115, 20);
		frame.getContentPane().add(textField_casa);
		textField_casa.setColumns(10);
		
	
		lblTimpLaCoada.setFont(new Font("Sitka Text", Font.BOLD, 13));
		lblTimpLaCoada.setBounds(25, 155, 108, 14);
		frame.getContentPane().add(lblTimpLaCoada);
		
		
		label_timpCasa.setFont(new Font("Sitka Text", Font.BOLD, 12));
		label_timpCasa.setBounds(25, 214, 96, 14);
		frame.getContentPane().add(label_timpCasa);
		

		lblNumarClienti.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNumarClienti.setBounds(25, 274, 96, 14);
		frame.getContentPane().add(lblNumarClienti);
		
	
		textField_totalClienti.setBounds(169, 271, 115, 20);
		frame.getContentPane().add(textField_totalClienti);
		textField_totalClienti.setColumns(10);
		frame.setVisible(true);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextField_Coada() {
		return textField_Coada;
	}

	public void setTextField_Coada(JTextField textField_Coada) {
		this.textField_Coada = textField_Coada;
	}

	public JTextField getTextField_casa() {
		return textField_casa;
	}

	public void setTextField_casa(JTextField textField_casa) {
		this.textField_casa = textField_casa;
	}

	public JTextField getTextField_totalClienti() {
		return textField_totalClienti;
	}

	public void setTextField_totalClienti(JTextField textField_totalClienti) {
		this.textField_totalClienti = textField_totalClienti;
	}

	public JTextPane getTextPane_simulare() {
		return textPane_simulare;
	}

	public void setTextPane_simulare(JTextPane textPane_simulare) {
		this.textPane_simulare = textPane_simulare;
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}


}
